const {Telegraf, session,Scenes:{Stage}} = require('telegraf')
const config = require('config')
const category = require('../model/Category')
const Product = require('../model/Product')
const productScene = require('./scene/productScene')

const bot = new Telegraf(config.get("BOT_TOKEN"))

const stage = new Stage([productScene])
bot.use(session())
bot.use(stage.middleware())

async function getCategories () {
    const products = await category.find({})
    return products.map(el => [{text:el.name,callback_data: `category_${el._id}`}])
    
}



bot.action(/category_(.+)/,ctx => {

ctx.scene.enter('productScene',{categoryId:ctx.match[1]})

})



bot.start( async ctx => {
    ctx.telegram.sendMessage(ctx.chat.id, 'Ovqatlarni tanlang ?',
    {
        reply_markup: JSON.stringify({
          inline_keyboard: await getCategories()
        }),
      }
    );
})



module.exports =  () =>  bot.launch()