const { Composer, Scenes, Markup } = require('telegraf')
const Product = require('../../model/Product')

const startStep = new Composer()


async function getProducts(id) {
    const products = await Product.find({ categoryId: id })
    return products.map(el => [{ text: el.name, callback_data: `product_${el._id}` }])

}


startStep.use(async ctx => {

    try {
        ctx.telegram.sendMessage(ctx.chat.id, "Ovqatlardan birini tanlang",
            {
                reply_markup: JSON.stringify({
                    inline_keyboard: await getProducts(ctx.scene.state.categoryId)
                }),
            }
        );

        ctx.wizard.next()


    }
    catch (e) {
    }
})

const affirmStep = new Composer()

affirmStep.action(/product_(.+)/,async ctx => {
    console.log(ctx.match[1])
    const dish = await Product.findById(ctx.match[1])
    if(dish) {
        ctx.replyWithPhoto(dish.image,{caption:dish.description},{
            reply_markup: JSON.stringify({
                inline_keyboard: [{text:"Buyurtma berish",callback_data: `product_${ctx.match[1]}`}]
            }),
        })
    }
   
}) 





const productScene = new Scenes.WizardScene('productScene', startStep, affirmStep)


module.exports = productScene