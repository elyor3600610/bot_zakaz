const express = require('express')
const startBot = require('./bot/bot.js')
const db = require('./db/db')
const category = require('./model/Category.js')


const app = express()
app.use(express.json())
app.use('/api',require('./routes/categoryRoute'))
app.use('/api',require('./routes/productRoute'))


 console.log(new category({
     name:"kreslo",
     categoryId :"61d6fefb43d3c1e911e567d0"
 }))


const Port = process.env.PORT || 5200


function startServer () {
db()
app.listen(Port,console.log("server is running"))
startBot()
console.log('Bot started')
}

startServer()