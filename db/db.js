const mongoose = require('mongoose')
const config = require('config')


const db =  async () => {
    try {
        mongoose.connect(config.get('MONGOURI'),{},console.log("Connected to db"))
    }
    catch(e) {
        console.log(e)
    }
    
} 

module.exports = db