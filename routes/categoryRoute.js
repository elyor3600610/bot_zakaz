
const {Router} = require('express')
const { add,getCategories, updateCategory } = require('../controller/categoryController')
const router = new Router()


router.post('/category/add',add)
router.get('/category/get/all',getCategories)
router.put('/category/update/:id',updateCategory)


module.exports = router