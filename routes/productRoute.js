
const {Router} = require('express')
const { add, getProducts, getProductById, updateProductById } = require('../controller/productController')

const router = new Router()


router.post('/product/add',add)
router.get('/product/get/all',getProducts)
router.get('/product/:id',getProductById)
router.put('/product/update/:id',updateProductById)


module.exports = router