const Product = require("../model/Product")

exports.add = async  (req,res) => {
    try {
        const product = new Product(req.body)
        const response  = await product.save()
        if(response) {
            res.status(201).json({
                success:true,
                data:response
            })
        }
    } 
    catch(e) {
        res.status(400).json({
            success:false,
            message:e
        })
    }
}

exports.getProducts =  async (req,res) => {
    try {
        const product = await Product.find({})
        if(product) {
            res.status(200).json({
                success:true,
                data:product
            })
        }
    } 
    catch(e) {
        res.status(400).json({
            success:false,
            message:e
        })
    }
}
exports.getProductById = async (req,res) => {
    try {

        const product = await Product.findById(req.params.id)
        if(product) {
            res.status(200).json({
                success:true,
                data:product
            })
        }
    } 
    catch(e) {
        res.status(400).json({
            success:false,
            message:e
        })
    }
}

exports.updateProductById = async (req,res) => {
    try {

        const product = await Product.findByIdAndUpdate(req.params.id,{$set:req.body},{new:true})
        if(product) {
            res.status(200).json({
                success:true,
                data:product
            })
        }
    } 
    catch(e) {
        res.status(400).json({
            success:false,
            message:e
        })
    }
}

