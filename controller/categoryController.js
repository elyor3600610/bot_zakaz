const category = require("../model/Category")

exports.add = async (req,res) => {
    try {
        const data = new category(req.body)
        const response  = await data.save()

        if(response) {
            res.status(201).json({
                success:true,
                data:response
            })
        }
    } 
    catch(e) {
      res.status(400).json({
            success:false,
            message:e
        })
    }
}

exports.getCategories =  async (req,res) => {
    try {
        const data = await category.find({})

        if(data) {
            res.status(200).json({
                success:true,
                data
            })
        }
    } 
    catch(e) {
        res.status(400).json({
            success:false,
            message:e
        })
    }
}
exports.updateCategory = async (req,res) => {
    try {
        const data = await category.findByIdAndUpdate(req.params.id,{$set:req.body},{new:true})
        if(data) {
            res.status(200).json({
                success:true,
                data
            })
        }
    } 
    catch(e) {
        res.status(400).json({
            success:false,
            message:e
        })
    }
}

